import pandas as pd  
import numpy as np  
import matplotlib.pyplot as plt  
import seaborn as seabornInstance 
from sklearn.model_selection import train_test_split 
from sklearn.linear_model import LinearRegression
from sklearn import metrics
df = pd.read_csv("Weather.csv")
pd.set_option('display.max_columns', len(df.columns))
withna = df.shape[1]
df = df.dropna(axis=1, how="all")
print("deleted", withna - df.shape[1], "cols.")
print(df.shape)
